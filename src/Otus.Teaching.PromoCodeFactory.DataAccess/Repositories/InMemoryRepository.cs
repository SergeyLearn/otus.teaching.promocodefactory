﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public async Task<Guid> AddAsync(T item)
        {
            item.Id = Guid.NewGuid();
            Data = Data.Append(item);
            return item.Id;
        }

        public async Task DeleteAsync(Guid id)
        {
            Data = Data.Where(d => d.Id != id);
        }

        public async Task UpdateAsync(Guid id, T item)
        {
            item.Id = id;
            Data = Data.Where(d => d.Id != id).Append(item);
        }
    }
}