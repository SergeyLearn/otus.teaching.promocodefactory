﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;

        public EmployeesController(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);
            if (employee == null)
                return NotFound();
            
            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }
    


        /// <summary>
        /// Добавить сотрудника
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<EmployeeCreateResponse>> Add([FromBody]EmployeeCreateRequest data)
        {
            Guid itemId = await _employeeRepository.AddAsync(CreateModel(data.FirstName, data.LastName, data.Email, data.AppliedPromocodesCount, data.Roles));
            return new EmployeeCreateResponse{ Id = itemId};
        }
        
        
        /// <summary>
        /// Отредактировать данные сотрудника
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<ActionResult> Update([FromBody]EmployeeEditRequest data)
        {
            await _employeeRepository.UpdateAsync(data.Id, CreateModel( data.FirstName, data.LastName, data.Email, data.AppliedPromocodesCount, data.Roles));
            return Ok();
        }

        /// <summary>
        /// Удалить сотрудника
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<ActionResult> Remove(Guid id)
        {
            await _employeeRepository.DeleteAsync(id);
            return Ok();
        }

        private Employee CreateModel(string firstName, string lastName, string email, int appliedPromocodesCount, List<EmployeeCreateRequest.EmployeeCreateRoleRequest> roles)
        {
            var rolesData = roles.Select(r => new Role{ Name = r.Name, Description = r.Description}).ToList();
            return new Employee
            {
                FirstName = firstName, LastName = lastName, Email = email,
                AppliedPromocodesCount = appliedPromocodesCount, Roles = rolesData
            };
        }
    }
}