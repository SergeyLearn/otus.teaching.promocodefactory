using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class EmployeeCreateRequest
    {
        [Required]
        [MinLength(1)]
        public string FirstName { get; set; }
        
        [Required]
        [MinLength(1)]
        public string LastName { get; set; }

        [Required]
        [MinLength(1)]
        public string Email { get; set; }

        public List<EmployeeCreateRoleRequest> Roles { get; set; }

        [Required]
        [Range(0, Int32.MaxValue)]
        public int AppliedPromocodesCount { get; set; }
        


        public class EmployeeCreateRoleRequest
        {
            public string Name { get; set; }
            public string Description { get; set; }
        }
    }
}